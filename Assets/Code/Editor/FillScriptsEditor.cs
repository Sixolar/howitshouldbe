﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using UnityEditor.SceneManagement;

namespace UnexTools
{
	public static class FillScriptsEditor
	{
		[MenuItem("Utility/Fill scripts")]
		public static void FillInspector()
		{
			GameObject[] selec = Selection.gameObjects;

			if (selec == null || selec.Length == 0)
			{
				Debug.Log("No object selected");
				return;
			}

			foreach (var go in selec)
			{
				foreach (var script in go.GetComponents<MonoBehaviour>())
				{
					var fields = script.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
					foreach (var f in fields)
					{
						if ((f.IsPublic || f.GetCustomAttribute(typeof(SerializeField)) != null) && typeof(Component).IsAssignableFrom(f.FieldType))
						{
							Component c = go.GetComponent(f.FieldType);
							if (c != null)
							{
								f.SetValue(script, c);
								Debug.Log(f.FieldType + " : " + f.Name + " set");
								EditorUtility.SetDirty(go);
							}
						}
					}
				}
			}

			EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
		}
	}
}
