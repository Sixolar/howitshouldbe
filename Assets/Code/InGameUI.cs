﻿using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
	[SerializeField]
	GameObject win = null;

	[SerializeField]
	GameObject lose = null;

	[SerializeField]
	Text timerText = null;

	public void SetTime(float time)
	{
		timerText.text = time.ToString("0.00");
	}

	public void Lose()
	{
		lose.SetActive(true);
	}

	public void Win()
	{
		win.SetActive(true);
	}

	private void OnDisable()
	{
		win.SetActive(false);
		lose.SetActive(false);
	}
}
