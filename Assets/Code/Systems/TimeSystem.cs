﻿using System;
using UnityEngine;

namespace UnexTools
{
	/// <summary>
	/// Static time event system
	/// Usage :
	///  Place on a DDOL object or any object in every scene where time event are used
	/// Note :
	///  Be careful to unregister destroyed objects
	/// </summary>
	public class TimeSystem : MonoBehaviour
	{
		public static Action<float> tic = delegate { };
		public static Action FUtic = delegate { };
		public static Action LUtic = delegate { };

		/// <summary>
		/// No events are fired if true
		/// </summary>
		public static bool paused = false;
		static float originalFixedDeltaTime = -1;

		public static void SetTimeScale(float scale)
		{
			if (originalFixedDeltaTime == -1) originalFixedDeltaTime = Time.fixedDeltaTime;
			Time.timeScale = scale;
			Time.fixedDeltaTime = originalFixedDeltaTime * scale;
		}

		private void Update()
		{
			if (!paused) tic(Time.deltaTime);
		}

		private void FixedUpdate()
		{
			if (!paused) FUtic();
		}

		private void LateUpdate()
		{
			if (!paused) LUtic();
		}
	}
}