﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnexTools
{
	/// <summary>
	/// Static class managing timers, needs a TimeSystem to work
	/// </summary>
	public static class TimerManager 
	{
		/// <summary>
		/// Pause time for scaled timers if true. Have no effect on all other timers, use TimeSystem.paused instead
		/// </summary>
		public static bool paused = false;

		/// <summary>
		/// Time multiplier for scaled timers. Have no effect on all other timers, use TimeSystem.SetTimeScale instead
		/// </summary>
		public static float timeScale = 1.0f;
		
		// list of currently active timers
		static List<Timer> timers = new List<Timer>();

		// list of currently active timers (that use time scale)
		static List<Timer> scaledTimers = new List<Timer>();

		// list of action to call next update
		static List<Action> nextU = new List<Action>();

		// list of action to call next fixedUpdate
		static List<Action> nextFU = new List<Action>();

		// list of action to call next lateUpdate
		static List<Action> nextLU = new List<Action>();

		static TimerManager()
		{
			TimeSystem.tic += Update;
			TimeSystem.FUtic += FixedUpdate;
			TimeSystem.LUtic += LateUpdate;
		}

		/// <summary>
		/// Create a new timer and start it
		/// </summary>
		/// <param name="interval"> timer's time interval </param>
		/// <param name="callback"> actions performed once the inteval has passed </param>
		/// <param name="looping"> is this timer looping </param>
		/// <param name="useTimeScale"> is this timer using scaled time </param>
		/// <returns> the timer created </returns>
		public static Timer NewTimer(float interval, Action callback, bool looping = false, bool useTimeScale = false)
		{
			Timer t = new Timer(interval, callback, looping);
			if (useTimeScale) scaledTimers.Add(t);
			else timers.Add(t);
			return t;
		}

		/// <summary>
		/// Add a timer to the running timer list
		/// </summary>
		/// <param name="t"></param>
		public static void AddTimer(Timer t)
		{
			if (!timers.Contains(t)) timers.Add(t);
		}

		/// <summary>
		/// Remove a timer from the running timer list
		/// </summary>
		/// <param name="t"></param>
		public static void RemoveTimer(Timer t)
		{
			timers.Remove(t);
		}

		/// <summary>
		/// Call the given action on next update (thread safe) 
		/// </summary>
		/// <param name="call"> Action to call next update </param>
		public static void CallNextUpdate(Action call)
		{
			nextU.Add(call);
		}

		/// <summary>
		/// Call the given action on next update, if it is not already schedule to do so (thread safe) 
		/// </summary>
		/// <param name="call"> Action to call next update </param>
		public static void CallOnceNextUpdate(Action call)
		{
			if (!nextU.Contains(call)) nextU.Add(call);
		}

		/// <summary>
		/// Call the given action on next fixed update (not thread safe) 
		/// </summary>
		/// <param name="call"> Action to call next fixed update </param>
		public static void CallNextFixedUpdate(Action call)
		{
			nextFU.Add(call);
		}

		/// <summary>
		/// Call the given action on next late update (not thread safe) 
		/// </summary>
		/// <param name="call"> Action to call next late update </param>
		public static void CallNextLateUpdate(Action call)
		{
			nextLU.Add(call);
		}
		
		private static void FixedUpdate()
		{
			for (int i = timers.Count - 1; i >= 0; i--)
			{
				// if the timer's tic returns true, it means that the timer is finished (and not looping) so remove it from the list
				if (timers[i].Tic(Time.fixedDeltaTime)) timers.RemoveAt(i);
			}

			if (!paused)
			{
				for (int i = scaledTimers.Count - 1; i >= 0; i--)
				{
					if (scaledTimers[i].Tic(Time.fixedDeltaTime * timeScale))
						scaledTimers.RemoveAt(i);
				}
			}

			for (int i = 0; i < nextFU.Count; i++) nextFU[i]();
			nextFU.Clear();
		}

		private static void LateUpdate()
		{
			for (int i = 0; i < nextLU.Count; i++) nextLU[i]();
			nextLU.Clear();
		}

		private static void Update(float delta)
		{
			for (int i = 0; i < nextU.Count; i++) nextU[i]();
			nextU.Clear();
		}
	}

	/// <summary>
	/// Timed event class, the tic function of each instance must be called in the game loop
	/// </summary>
	public class Timer {

		// interval between two trigger 
		float timeout;
		// current remaining time
		public float RemainingTime { get; private set; }
		// event called when timer reaches 0
		Action call;
		// whether or not the timer start over when it reaches 0
		bool loop;

		/// <summary>
		/// Create a new timer instance
		/// </summary>
		/// <param name="interval"> timer's time interval </param>
		/// <param name="callback"> actions performed once the inteval has passed </param>
		/// <param name="looping"> is this timer looping </param>
		public Timer(float interval, Action callback, bool looping = false)
		{
			timeout = interval;
			RemainingTime = timeout;
			call = callback;
			loop = looping;
		}

		/// <summary>
		/// Update the time remaining of the timer, calling the associated event if it reaches 0
		/// </summary>
		/// <param name="time"> how much time to substract from the remaining time </param>
		/// <returns> true if the timer is over and not looping </returns>
		public bool Tic(float time)
		{
			RemainingTime -= time;
			if (RemainingTime <= 0)
			{
				try {
					call();
					if (loop) RemainingTime += timeout;
					else return true;
				}
				catch (Exception e) {
					Debug.LogError(e.ToString());
					return true;
				}
			}
			return false;
		}

		public void Reset()
		{
			RemainingTime = timeout;
		}

		public void Reset(float newTimeout)
		{
			timeout = newTimeout;
			RemainingTime = timeout;
		}

		public void AddCallback(Action callback)
		{
			call += callback;
		}

		public void SetCallback(Action callback)
		{
			call = callback;
		}
	}
}