﻿using System.Collections;
using System.Collections.Generic;
using UnexTools;
using UnityEngine;
using UnityEngine.UI;

public class GameSystem : MonoBehaviour
{
	[SerializeField]
	GameObject mainMenu = null;

	[SerializeField]
	InGameUI ingameUI = null;

	[SerializeField]
	GameObject finishedGame = null;

	[SerializeField]
	List<LevelInfo> levelPrefabs = new List<LevelInfo>();

	float timer = 0;

	private void Awake()
	{
		Clear();
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Clear()
	{
		GameManager.todoLevels = new List<LevelInfo>(levelPrefabs);
	}

	public void Play()
	{
		if (GameManager.todoLevels.Count > 0)
		{
			int id = Random.Range(0, GameManager.todoLevels.Count);
			GameManager.currentLevel = Instantiate(GameManager.todoLevels[id]);
			GameManager.currentLevel.id = id;
			GameManager.currentLevel.gameObject.SetActive(true);
			mainMenu.SetActive(false);
			ingameUI.gameObject.SetActive(true);
			timer = 20;
		}
		else finishedGame.SetActive(true);
	}

	public void ReturnToMain(GameObject level)
	{
		mainMenu.SetActive(true);
		ingameUI.gameObject.SetActive(false);
		Destroy(level);
	}

	private void Update()
	{
		if (GameManager.currentLevel != null)
		{
			if (GameManager.currentLevel.done)
			{
				ingameUI.Win();
				GameManager.todoLevels.RemoveAt(GameManager.currentLevel.id);
				var level = GameManager.currentLevel;
				GameManager.currentLevel = null;
				TimerManager.NewTimer(4, delegate { ReturnToMain(level.gameObject); });
			}
			else
			{
				timer -= Time.deltaTime;
				if (timer <= 0)
				{
					ingameUI.SetTime(0);
					ingameUI.Lose();
					var level = GameManager.currentLevel;
					GameManager.currentLevel = null;
					TimerManager.NewTimer(2.2f, delegate { ReturnToMain(level.gameObject); });
				}
				else ingameUI.SetTime(timer);
			}
		}
	}
}
