﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeOnRelease : MonoBehaviour, IEndDragHandler
{
	[SerializeField]
	List<SpriteScripts> toEnable = new List<SpriteScripts>();

	[SerializeField]
	List<SpriteScripts> toDisable = new List<SpriteScripts>();

	[SerializeField]
	SpriteScripts trigger = null;

	[SerializeField]
	bool isWinCondition = false;

	[SerializeField]
	List<MonoBehaviour> enableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	List<MonoBehaviour> disableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	AudioSource playOnTrigger = null;

	private void Awake()
	{
		if (isWinCondition)
		{
			if (GameManager.currentLevel != null) GameManager.currentLevel.actionCount++;
		}
	}

	private void Update()
	{
		
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (enabled)
		{
			if (trigger.col.bounds.IntersectRay(Camera.main.ScreenPointToRay(Input.mousePosition)))
			{
				foreach (var o in toDisable) o.gameObject.SetActive(false);
				foreach (var o in toEnable) o.gameObject.SetActive(true);
				foreach (var c in enableOnChange) c.enabled = true;
				foreach (var c in disableOnChange) c.enabled = false;
				if (playOnTrigger != null) playOnTrigger.Play();
				if (isWinCondition) GameManager.currentLevel.actionCount--;
			}
		}
	}
}
