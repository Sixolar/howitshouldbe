﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScripts : MonoBehaviour
{
	public Collider2D col = null;
	
	public Rigidbody2D rgbd = null;
	
	public SpriteRenderer rdr = null;
	
	public LinkedSprite link = null;

	public DragSprite drag = null;
}
