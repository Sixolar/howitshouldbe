﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAtChildCount : MonoBehaviour
{
	[SerializeField]
	SpriteScripts target = null;

	[SerializeField]
	List<SpriteScripts> toEnable = new List<SpriteScripts>();

	[SerializeField]
	List<SpriteScripts> toDisable = new List<SpriteScripts>();

	[SerializeField]
	int count = 1;

	[SerializeField]
	List<MonoBehaviour> enableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	List<MonoBehaviour> disableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	AudioSource playOnTrigger = null;

	private void Update()
	{
		if (target.link.children.Count == count)
		{
			foreach (var o in toDisable) o.gameObject.SetActive(false);
			foreach (var o in toEnable) o.gameObject.SetActive(true);
			foreach (var c in enableOnChange) c.enabled = true;
			foreach (var c in disableOnChange) c.enabled = false;
			if (playOnTrigger != null) playOnTrigger.Play();
		}
	}
}
