﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnlinkOnDrag : MonoBehaviour, IBeginDragHandler
{
	[SerializeField]
	SpriteScripts target = null;

	[SerializeField]
	SpriteScripts parent = null;

	[SerializeField]
	int newOrder = 0;

	public void OnBeginDrag(PointerEventData eventData)
	{
		parent.link.RemoveChild(target);
		target.rdr.sortingOrder = newOrder;
	}
}
