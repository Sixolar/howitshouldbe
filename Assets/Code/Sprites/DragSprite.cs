﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragSprite : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[SerializeField]
	SpriteScripts sprite = null;

	[SerializeField]
	AudioSource playOnBegin = null;

	[SerializeField]
	AudioSource playOnEnd = null;

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (playOnBegin != null) playOnBegin.Play();
	}

	public void OnDrag(PointerEventData eventData)
	{
		SpriteManager.currentDrag = sprite;
		sprite.rgbd.constraints = RigidbodyConstraints2D.FreezeAll;
		var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		pos.z = 0;
		transform.position = pos;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (playOnEnd != null) playOnEnd.Play();
		SpriteManager.currentDrag = null;
		sprite.rgbd.constraints = RigidbodyConstraints2D.None;
	}
}
