﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LinkOnRelease : MonoBehaviour, IEndDragHandler
{
	[SerializeField]
	SpriteScripts scripts = null;

	[SerializeField]
	SpriteScripts linkTo = null;

	[SerializeField]
	int targetOrder = 0;

	[SerializeField]
	bool disable = false;

	[SerializeField]
	bool snap = false;

	[SerializeField]
	Vector3 snapOffset = Vector3.zero;

	[SerializeField]
	bool isWinCondition = false;

	[SerializeField]
	List<MonoBehaviour> enableOnLink = new List<MonoBehaviour>();

	[SerializeField]
	List<MonoBehaviour> disableOnLink = new List<MonoBehaviour>();

	[SerializeField]
	AudioSource playOnTrigger = null;

	private void Awake()
	{
		if (isWinCondition)
		{
			if (GameManager.currentLevel != null) GameManager.currentLevel.actionCount++;
		}
	}

	private void Update()
	{
		
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (enabled)
		{
			if (linkTo.col.bounds.IntersectRay(Camera.main.ScreenPointToRay(Input.mousePosition)))
			{
				if (snap)
				{
					transform.position = linkTo.transform.position + snapOffset;
					transform.rotation = linkTo.transform.rotation;
				}
				scripts.rdr.sortingOrder = targetOrder;
				if (scripts.link != null) foreach (var c in scripts.link.children) c.rdr.sortingOrder = targetOrder;
				linkTo.link.AddChild(scripts);
				if (disable) scripts.rgbd.simulated = false;
				foreach (var c in enableOnLink) c.enabled = true;
				foreach (var c in disableOnLink) c.enabled = false;
				if (playOnTrigger != null) playOnTrigger.Play();
				if (isWinCondition) GameManager.currentLevel.actionCount--;
			}
		}
	}
}
