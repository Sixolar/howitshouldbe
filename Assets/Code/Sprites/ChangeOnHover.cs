﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOnHover : MonoBehaviour
{
	[SerializeField]
	List<SpriteScripts> toEnable = new List<SpriteScripts>();

	[SerializeField]
	List<SpriteScripts> toDisable = new List<SpriteScripts>();

	[SerializeField]
	SpriteScripts requiredToHold = null;

	[SerializeField]
	SpriteScripts trigger = null;

	[SerializeField]
	AudioSource playOnTrigger = null;

	private void Update()
	{
		if (SpriteManager.currentDrag == requiredToHold)
		{
			if (trigger.col.bounds.IntersectRay(Camera.main.ScreenPointToRay(Input.mousePosition)))
			{
				foreach (var o in toDisable) o.gameObject.SetActive(false);
				foreach (var o in toEnable) o.gameObject.SetActive(true);
				if (playOnTrigger != null) playOnTrigger.Play();
			}
			else
			{
				foreach (var o in toDisable) o.gameObject.SetActive(true);
				foreach (var o in toEnable) o.gameObject.SetActive(false);
			}
		}
	}
}
