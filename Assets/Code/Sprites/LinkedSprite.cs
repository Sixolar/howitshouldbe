﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedSprite : MonoBehaviour
{
	public List<SpriteScripts> children = new List<SpriteScripts>();

	List<Vector3> offsets = new List<Vector3>();

	private void Awake()
	{
		foreach (var c in children) offsets.Add(c.transform.position - transform.position);
	}

	private void Update()
	{
		for (int i = 0; i < children.Count; i++) children[i].transform.position = transform.position + offsets[i];
	}

	public void AddChild(SpriteScripts child)
	{
		child.rgbd.constraints = RigidbodyConstraints2D.FreezeAll;
		children.Add(child);
		offsets.Add(child.transform.position - transform.position);
	}

	public void RemoveChild(SpriteScripts child)
	{
		int i = -1;
		if ((i = children.IndexOf(child)) >= 0)
		{
			child.rgbd.constraints = RigidbodyConstraints2D.None;
			children.RemoveAt(i);
			offsets.RemoveAt(i);
		}
	}

	private void OnDisable()
	{
		foreach (var c in children) c.gameObject.SetActive(false);
	}
}
