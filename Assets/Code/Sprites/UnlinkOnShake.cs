﻿using System.Collections;
using System.Collections.Generic;
using UnexTools;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnlinkOnShake : MonoBehaviour, IBeginDragHandler, IDragHandler
{
	[SerializeField]
	SpriteScripts target = null;

	[SerializeField]
	SpriteScripts parent = null;

	[SerializeField]
	int newOrder = 0;
	
	[SerializeField]
	float shakeTime = 1;

	[SerializeField]
	Vector2 ejectForce = Vector2.zero;

	[SerializeField]
	List<MonoBehaviour> enableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	List<MonoBehaviour> disableOnChange = new List<MonoBehaviour>();

	[SerializeField]
	AudioSource playOnTrigger = null;

	[SerializeField]
	AudioSource playOnBegin = null;

	List<Vector2> changes = new List<Vector2>();
	Vector3 lastPos = Vector3.zero;
	float timer = 0;
	bool shaking = false;
	
	private void Update()
	{
		if (shaking)
		{
			if ((timer += Time.deltaTime) >= shakeTime)
			{
				timer = 0;

				parent.link.RemoveChild(target);
				target.rdr.sortingOrder = newOrder;
				target.rgbd.AddForce(ejectForce, ForceMode2D.Impulse);
				foreach (var c in enableOnChange) c.enabled = true;
				foreach (var c in disableOnChange) c.enabled = false;
				if (playOnTrigger != null) playOnTrigger.Play();
			}
		}
		else timer = 0;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (enabled)
		{
			Vector2 change = transform.position - lastPos;
			lastPos = transform.position;
			changes.Add(change);
			TimerManager.NewTimer(0.2f, delegate { changes.Remove(change); });

			float posX = 0;
			float negX = 0;
			float posY = 0;
			float negY = 0;
			foreach (var c in changes)
			{
				if (c.x > 0) posX += c.x;
				else negX -= c.x;
				if (c.y > 0) posY += c.y;
				else negY -= c.y;
			}

			if ((posX > 5 && negX > 5) || (posY > 5 && negY > 5))
			{
				if (playOnBegin != null && !playOnBegin.isPlaying) playOnBegin.Play();
				shaking = true;
			}
			else
			{
				if (playOnBegin != null && playOnBegin.isPlaying) playOnBegin.Stop();
				shaking = false;
			}
			
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (enabled)
		{
			lastPos = transform.position;
		}
	}
}
