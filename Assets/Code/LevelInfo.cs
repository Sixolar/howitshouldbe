﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo : MonoBehaviour
{
	public int id;
	public int actionCount = 0;
	public bool done { get { return actionCount == 0; } }
}
